from sys import argv, exit
from pathlib import Path

def process_file(o_path):
    if o_path.suffix != '.m':
        print(f'Warn: Path {o_path} is not an objC file.')
        return
    insert_log = 0
    str_file = ''
    with o_path.open() as f:
        for line in f:
            if insert_log == 0:
                if (line[0] == '-' or line[0] == '+') and ('(' in line[1:5]):
                    insert_log = 1 
            if insert_log == 1:
                if '{' in line:
                    insert_log = 2
            str_file += line
            if insert_log == 2:
                insert_log = 0
                str_file += r'    NSLog(@"[%s]:: ", __func__);'
                str_file += '\n'

    print(f'Writing file {o_path}:')
    # print(str_file)
    o_path.write_text(str_file)

def process_dir(o_path):
    for child in o_path.iterdir():
        if child.is_file():
            process_file(child)
    
def check_arg(str_fs_node):
    node_path = Path(str_fs_node)
    if not node_path.exists():
        print(f'Path {str_fs_node} doesn\'t exist.')
        return
    if node_path.is_file():
        process_file(node_path)
    elif node_path.is_dir():
        process_dir(node_path)

def main():
    if len(argv) == 1:
        print('Pass file(s) or folder(s) to script')
        exit(0)

    for arg in argv[1:]:
        check_arg(arg)

main()
