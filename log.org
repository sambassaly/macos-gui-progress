VLC: macOS gui -*- mode: org -*-

** TODO Browse view
   - Preliminary investigation
     [2021-09-21 Tue]
   - Started working on children generation for local items
     [2021-09-22 Wed]
     [2021-09-23 Thu]
     [2021-09-24 Fri]
     [2021-09-26 Sun]

   - Implementing Supplementary view for browsing
     - [x] add flow layout
     - [x] Create custom supplementary view
     - [x] implement custom layout attributes
       - [x] investigate why sup view is not collapsed, and why it doesn't expand the first time
     - [x] animate expansion and collapse
     - [x] fix a bug with the new width after expansion 
     - [ ] use invalidate with context
     - [ ] implement view looks
     - [ ] implement view contents
     - [ ] integrate with media lib

   :LOGBOOK:
   CLOCK: [2022-01-23 Sun 20:31]--[2022-01-23 Sun 23:53] =>  3:22
   CLOCK: [2022-01-18 Tue 18:31]--[2022-01-18 Tue 22:59] =>  4:28
   CLOCK: [2021-12-02 Thu 18:32]--[2021-12-02 Thu 23:30] =>  4:58 
   CLOCK: [2021-11-30 Tue 18:20]--[2021-11-30 Tue 23:14] =>  4:54
   CLOCK: [2021-11-25 Thu 18:11]--[2021-11-25 Thu 23:14] =>  5:03
   CLOCK: [2021-11-23 Tue 18:16]--[2021-11-23 Tue 22:53] =>  4:37
   CLOCK: [2021-10-30 Sat 06:05]--[2021-10-30 Sat 10:15] =>  4:10
   CLOCK: [2021-10-28 Thu 20:02]--[2021-10-28 Thu 23:03] =>  3:01
   CLOCK: [2021-10-21 Thu 20:40]--[2021-10-21 Thu 23:25] =>  2:45
   CLOCK: [2021-10-19 Tue 19:09]--[2021-10-19 Tue 23:39] =>  4:30
   CLOCK: [2021-10-13 Wed 18:33]--[2021-10-13 Wed 23:37] =>  5:04
   CLOCK: [2021-09-26 Sun 20:22]--[2021-09-26 Sun 22:09] =>  1:47
   CLOCK: [2021-09-26 Sun 17:09]--[2021-09-26 Sun 19:12] =>  2:03
   CLOCK: [2021-09-24 Fri 21:30]--[2021-09-24 Fri 23:05] =>  1:35
   CLOCK: [2021-09-23 Thu 21:06]--[2021-09-23 Thu 22:47] =>  1:41
   CLOCK: [2021-09-22 Wed 20:12]--[2021-09-22 Wed 22:10] =>  1:58
   CLOCK: [2021-09-21 Tue 21:32]--[2021-09-21 Tue 22:35] =>  1:03
   :END:
   
   
   

** TODO Bottom Bar
   
   Progress is on branch: ~bottombar~.

   :LOGBOOK:
   CLOCK: [2021-09-21 Tue 19:29]--[2021-09-21 Tue 21:32] =>  2:03
   CLOCK: [2021-09-18 Sat 23:48]--[2021-09-19 Sun 01:03] =>  1:15
   :END:

**** DONE Update time field to show passed or remaining and total
**** DONE Fix bug with flickering NSImageView in now playing
     

** DONE Issue 24746

   :PROPERTIES:
   :ORDERED:  t



   :END:
   :LOGBOOK:
   CLOCK: [2021-09-17 Fri 21:10]--[2021-09-17 Fri 21:58] =>  0:48

   Modified the fix as per the review.


   CLOCK: [2021-09-14 Tue 19:14]--[2021-09-14 Tue 22:13] =>  2:59

   Issue 24746 finished.
   Started RTL issue.

   CLOCK: [2021-09-11 Sat 16:21]--[2021-09-11 Sat 20:09] =>  3:48

   Re-compile is now fixed, issue re-produced. under investigation.

   CLOCK: [2021-09-10 Fri 19:48]--[2021-09-11 Sat 01:31] =>  5:43

   Re-compile trial and fail.

   :END:

** TODO Issue 25559
   :LOGBOOK:
   CLOCK: [2021-09-18 Sat 23:47]--[2021-09-18 Sat 23:48] =>  0:01
   CLOCK: [2021-09-18 Sat 21:07]--[2021-09-18 Sat 22:45] =>  1:38
   
   Investigation + some trials
   To be continued.

   CLOCK: [2021-09-17 Fri 21:59]--[2021-09-17 Fri 23:42] =>  1:43
   
   Testing with various approaches.
   Understanding what is being done in prefs.m and prefs_widgets.m 

   :END:




