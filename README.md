# macOS GUI Implementation Plan

[[_TOC_]]

#### Issues

- [x] [\#24746](https://code.videolan.org/videolan/vlc/-/issues/24746): VLCMain sharedInstance can recursively call itself and crash  
- [ ] [\#25559](https://code.videolan.org/videolan/vlc/-/issues/25559): Advanced preferences wrong tree alignment in RTL  
- [ ] [\#22825](https://code.videolan.org/videolan/vlc/-/issues/22825): Add player time support  
- [ ] [\#23142](https://code.videolan.org/videolan/vlc/-/issues/23142): medialibrary: Do preparsing on low priority background threads  

#### Tabs

- All:
  - Unify the view by creating a VLCCollctionViewFlowLayout for collection views  
  - Fix background color to use System's color in both modes.  
  - Do the same for table views  
  - Update buttons looks.  
  - _Implement sub item [view](https://zpl.io/aMNold3) (big task)_  
- Videos:  
  - Fully implement Recently watched  
- Video + Audio:  
  - Add transparency on scroll for the view holding sub-categories.  
  - Investigate: Add a way to Remove from Media Library.  
- Browse View:  
  - Implement correct behaviour when the object opens or not.  
  - Finalize implementing browsing local files.   
  - Add `Add to media library` to the view.  
  - Implement the path viewer.

#### Bottom bar

- [ ] Update play button  
- [x] Minor fix in the small album preview in the main GUI    
- [ ] Add variations in the bottom bar to the play view
  - [ ] Buttons instead of album preview  
  - [ ] Transparency when playing videos
- [x] Investigate a flickering issue with the album art

#### Play View

- For Music:  
  - Music View (NSVisualEffectsView (translucency) + album artwork)  
  - Extra buttons below Album artwork  
- For Video:  
  Nothing for now



#### General

- Implement correct look for the upper toolbar.  
- Implement side bar.  
- Correct functionality for back button.



