# To compile 

## Pre-req

Add to line 44 in `/Users/shkshk90/Dev/vlc/modules/codec/avcodec/avcommon.h`
```C
# include <libavutil/mathematics.h>
```

Set line 1113 in ``
```C
uint8_t *av_packet_sidedata = av_packet_get_side_data(packet, AV_PKT_DATA_QUALITY_FACTOR, NULL);
```

And use the systems google protobuf, don't use the downloaded includes or libs.
On line 47 of file `contrib/x86_64-apple-darwin15/include/libavutil/bswap.h`:
```c++
#elif ARCH_X86
#   if defined(__APPLE__)
// Mac OS X / Darwin features
#       include <libkern/OSByteOrder.h>
#       define bswap_16(x) OSSwapInt16(x)
#       define bswap_32(x) OSSwapInt32(x)
#       define bswap_64(x) OSSwapInt64(x)
#   else
#       include "x86/bswap.h"
#   endif
#endif
```

# After each update to master branch, 

### New 

```sh
cd contrib/contrib-osx
export OSX_VERSION=10.11
#
$ rm -rf a52dec aribb24 asdcplib bitstream breakpad cddb chromaprint \
dvdnav dvdread ebml faad2 ffmpeg fluidsynth game-music-emu gettext glslang \
gmp gnutls goom gsm jpeg lame libarchive libdca libdvbpsi libebur128 \
libkate libmodplug libmpeg2 libplacebo librist live555 matroska \
mpg123 musepack mysofa nettle nfs openjpeg rnnoise schroedinger sidplay-libs \
smb2 sparkle spatialaudio speexdsp sqlite srt ssh2 twolame upnp vncclient zvbi \
medialibrary
#
$ make .gettext
#
$ make a52dec aribb24 asdcplib bitstream breakpad cddb chromaprint \
dvdnav dvdread ebml faad2 ffmpeg fluidsynth game-music-emu glslang \
gmp gnutls goom gsm jpeg lame libarchive libdca libdvbpsi libebur128 \
libkate libmodplug libmpeg2 libplacebo librist live555 matroska \
mpg123 musepack mysofa nettle nfs openjpeg rnnoise schroedinger sidplay-libs \
smb2 sparkle spatialaudio speexdsp sqlite srt ssh2 twolame upnp vncclient zvbi
#
$ make medialibrary
```

### this is old 
##### FROM HERE
run the following commands in the main dir

```sh
export OSX_VERSION=10.11
mkdir -p contrib/contrib-osx && cd contrib/contrib-osx
../bootstrap --build=x86_64-apple-darwin15
make prebuilt

mv contrib/x86_64-apple-darwin15/include/google contrib/x86_64-apple-darwin15/include/__google
rm contrib/x86_64-apple-darwin15/lib/libproto*
mv contrib/x86_64-apple-darwin15/include/libavcodec contrib/x86_64-apple-darwin15/include/__libavcodec
mv contrib/x86_64-apple-darwin15/include/libavformat contrib/x86_64-apple-darwin15/include/__libavformat
mv contrib/x86_64-apple-darwin15/include/libavutil contrib/x86_64-apple-darwin15/include/__libavutil
rm contrib/x86_64-apple-darwin15/lib/libav*
```

then proceed
#### TILL HERE

## Compile command

From build dir
To configure:

```sh
../extras/package/macosx/configure.sh --enable-debug --host=x86_64-apple-darwin19 --with-macosx-sdk=`xcrun --show-sdk-path` --with-contrib=$PWD/../contrib/x86_64-apple-darwin19

# Then
./compile

# Then
make VLC.app
```
## Changes to system

```sh
brew unlink ffmpeg 
brew unlink protobuf
brew unlink gmp # probably
brew unlink gnutls
brew unlink libidn
brew unlink dbus
brew unlink glib 
brew unlink gettext
brew unlink harfbuzz
brew unlink ginstall
brew unlink autoconf
```

## After each pull, to fix the issues with gettext

you can build gettext by doing the following
```sh
mkdir -p contrib/contrib-osx 
cd contrib/contrib-osx 
make fetch
make -j4 .gettext
cd ../..            # back to vlc
cp contrib/x86_64-apple-darwin19/bin/gett* extras/tools/build/bin/
```

## Bison error

```sh
export PATH="/usr/local/opt/bison/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/bison/lib"
```

## How to compile from scratch

First, the dependencies

```sh
export OSX_VERSION=10.11
export VALID_ARCHS=x86_64
export ONLY_ACTIVE_ARCH=YES
mkdir -p contrib/contrib-osx && cd contrib/contrib-osx
../bootstrap --build=x86_64-apple-darwin15
make fetch 

make -j4 gettext
cd ../..            # back to vlc
cp contrib/x86_64-apple-darwin19/bin/gett* extras/tools/build/bin/
cd - # Back to contrib-osx


brew unlink a52dec; \
brew unlink aribb24; \
brew unlink asdcplib; \
brew unlink bitstream; \
brew unlink breakpad; \
brew unlink cddb; \
brew unlink chromaprint; \
brew unlink dvdnav; \
brew unlink dvdread; \
brew unlink ebml; \
brew unlink faad2; \
brew unlink fluidsynth; \
brew unlink game-music-emu; \
brew unlink gettext; \
brew unlink glslang; \
brew unlink gmp; \
brew unlink gnutls; \
brew unlink goom; \
brew unlink gsm; \
brew unlink jpeg; \
brew unlink lame; \
brew unlink libarchive; \
brew unlink libdca; \
brew unlink libdvbpsi; \
brew unlink libebur128; \
brew unlink libkate; \
brew unlink libmodplug; \
brew unlink libmpeg2; \
brew unlink libplacebo; \
brew unlink librist; \
brew unlink live555; \
brew unlink matroska; \
brew unlink medialibrary; \
brew unlink mpg123; \
brew unlink musepack; \
brew unlink mysofa; \
brew unlink nettle; \
brew unlink nfs; \
brew unlink openjpeg; \
brew unlink protobuf; \
brew unlink rnnoise; \
brew unlink schroedinger; \
brew unlink sidplay-libs; \
brew unlink smb2; \
brew unlink sparkle; \
brew unlink spatialaudio; \
brew unlink speexdsp; \
brew unlink sqlite; \
brew unlink srt; \
brew unlink ssh2; \
brew unlink twolame; \
brew unlink upnp; \
brew unlink vncclient; \
brew unlink zvbi; \
brew unlink ffmpeg;

make -j4
make .ffmpeg
make .twolame
make .sparkle
make .jpeg
make .medialibrary

cd ../../build
make clean
../extras/package/macosx/configure.sh --enable-debug --host=x86_64-apple-darwin19 --with-macosx-sdk=`xcrun --show-sdk-path` --with-contrib=$PWD/../contrib/x86_64-apple-darwin19

./compile
make -j4 VLC.app

cd .. # VLC
cd build && make -j4 VLC.app && lldb VLC.app; cd ..
```

#### To set language to RTL

```sh
defaults write org.videolan.vlc AppleLanguages '(ar)'
# to reset 
defaults write org.videolan.vlc AppleLanguages 'Base'
```

## To add artwork to mp3
```sh
$ lame --ti /path/to/file.jpg audio.mp3
```